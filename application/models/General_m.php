<?php 
class General_m extends CI_Model{
    public function __construct(){
        parent::__construct();
    }

    public function getData($table){
        $this->db->from($table);
        $this->db->order_by('id', "desc");
        return $this->db->get()->result(); 
    }

    public function insertData($table, $data){
        $this->db->insert($table, $data);
    }

    public function getWhere($table, $data, $cond){
        return $this->db->get_where($table, $data)->$cond();
    }

    public function updateData($table, $data, $keyID, $Valid){
        $this->db->where($keyID, $Valid);
        $this->db->update($table, $data);
    }

    public function getLimit($table, $cond, $key, $value, $limit){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by($key, $value);
        $this->db->limit($limit);
        return $this->db->get()->$cond();
    }

    public function deleteData($table, $keyID, $Valid)
    {
        $this->db->where($keyID, $Valid);
        $this->db->delete($table);
    }

    public function getJoinPegawai()
    {
        $this->db->select('pegawai.*, departemen.nama as dep_nama');
        $this->db->from('pegawai');
        $this->db->join('departemen', 'departemen.id = pegawai.dep_id');
        return $this->db->get()->result();
    }
  
    public function getJoinBarang()
    {
        $this->db->select('barang.*, jenis_barang.nama as jenis_nama, kategori.nama as kategori_nama,');
        $this->db->from('barang');
        $this->db->join('kategori', 'kategori.id = barang.kategori_id');
        $this->db->join('jenis_barang', 'jenis_barang.id = barang.jenis_id');
        return $this->db->get()->result();
    }

}





?>