    <!-- ============================================================== -->
    <!-- data table  -->
    <!-- ============================================================== -->

    <?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-primary alert-dismissible fade show" role="alert">
        <strong> Sukses !</strong> <?= $this->session->flashdata('success'); ?>
        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </a>
    </div>
    <?php } ?>
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 float-left">Data Departemen</h5>
            <a href="<?= base_url('departemen/add') ?>" class="btn btn-primary btn-sm float-right"><i
                    class="fas fa-plus"></i> Tambah departemen</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered second" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Abbr</th>
                            <th>Alamat</th>
                            <th>Telpon</th>
                            <th style="width:20%">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach ($data as $d) { ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $d->nama ?></td>
                            <td><?= $d->abbr ?></td>
                            <td><?= $d->alamat ?></td>
                            <td><?= $d->telpon ?></td>
                            <td>
                                <a href="<?= base_url('departemen/edit/'.$d->id) ?>" class="btn btn-warning btn-sm"><i
                                class="fas fa-pencil-alt"></i> Edit</a>
                                <a onclick="return confirm('Apakah data ini akan dihapus ?')" href="<?= base_url('departemen/delete/'.$d->id) ?>" class="btn btn-danger btn-sm"><i
                                class="fas fa-trash"></i> Hapus</a>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end data table  -->
    <!-- ============================================================== -->
<!-- 
    <button class="btn btn-default" id="btn-confirm">Confirm</button>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="mi-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmar</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" id="modal-btn-si">Si</button>
        <button type="button" class="btn btn-primary" id="modal-btn-no">No</button>
      </div>
    </div>
  </div>
</div>

<div class="alert" role="alert" id="result"></div>

<script>

var modalConfirm = function(callback){
  
  $("#btn-confirm").on("click", function(){
    $("#mi-modal").modal('show');
  });

  $("#modal-btn-si").on("click", function(){
    callback(true);
    $("#mi-modal").modal('hide');
  });
  
  $("#modal-btn-no").on("click", function(){
    callback(false);
    $("#mi-modal").modal('hide');
  });
};

modalConfirm(function(confirm){
  if(confirm){
    //Acciones si el usuario confirma
    $("#result").html("CONFIRMADO");
  }else{
    //Acciones si el usuario no confirma
    $("#result").html("NO CONFIRMADO");
  }
});
</script> -->