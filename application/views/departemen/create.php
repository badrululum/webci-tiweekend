<div class="row">
    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Form Departemen</h5>
            <div class="card-body">
                <form action="<?= base_url('departemen/add') ?>" method="post">
                    <div class="form-group">
                        <label for="nama"> Nama</label>
                        <input id="nama" type="text" name="nama" data-parsley-trigger="change" required
                            placeholder="Masukan nama" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="abbr"> Abbr</label>
                        <input id="abbr" type="text" name="abbr" data-parsley-trigger="change" required
                            placeholder="Masukan Abbr"   class="form-control">
                    </div>

                    
                    <div class="form-group">
                        <label for="alamat"> Alamat</label>
                        <textarea id="alamat" name="alamat" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="telepone"> Telepone</label>
                        <input id="telepone" type="text" name="telepone" data-parsley-trigger="change" required
                            placeholder="Masukan Abbr"   class="form-control">
                    </div>
                    

                    <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-primary"><i class="fas fa-location-arrow"></i>
                            Simpan</button>
                        <a href="<?= base_url('pegawai') ?>" class="btn btn-warning"><i class="fas fa-history"></i> Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>