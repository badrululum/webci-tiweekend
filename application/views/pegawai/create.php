<div class="row">
    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Form Pegawai</h5>
            <div class="card-body">
                <form action="<?= base_url('pegawai/add') ?>" method="post">
                    <div class="form-group">
                        <label for="nip"> NIP</label>
                        <input id="nip" type="text" name="nip" data-parsley-trigger="change" required
                            placeholder="Masukan nip" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nama"> Nama</label>
                        <input id="nama" type="text" name="nama" data-parsley-trigger="change" required
                            placeholder="Masukan nama" class="form-control">
                    </div>
                    <div class="form-group">
                        <label> Gender</label> <br>
                        <label class="custom-control custom-radio custom-control-inline">
                            <input type="radio" name="gender" value="L" class="custom-control-input"><span class="custom-control-label">Laki laki</span>
                        </label><br>
                        <label class="custom-control custom-radio custom-control-inline">
                            <input type="radio" name="gender" value="P" class="custom-control-input"><span class="custom-control-label">Perempuan</span>
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="tmp_lahir"> Tempat lahir</label>
                        <input id="tmp_lahir" type="text" name="tmp_lahir" data-parsley-trigger="change" required
                            placeholder="Masukan tempat lahir"   class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="datetimepicker4"> Tanggal lahir</label>
                        <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                            <input type="text" name="tgl_lahir" class="form-control datetimepicker-input"
                                data-target="#datetimepicker4" />
                            <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="alamat"> Alamat</label>
                        <textarea id="alamat" name="alamat" class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="dep_id">Departemen</label>
                        <select class="form-control" id="dep_id" name="dep_id">
                            <option selected disabled> --pilih departemen-- </option>
                            <?php foreach($departemen as $d) { ?>
                                <option value="<?= $d->id ?>"><?= $d->nama ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-primary"><i class="fas fa-location-arrow"></i>
                            Simpan</button>
                        <a href="<?= base_url('pegawai') ?>" class="btn btn-warning"><i class="fas fa-history"></i> Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>