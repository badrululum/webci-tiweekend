    <!-- ============================================================== -->
    <!-- data table  -->
    <!-- ============================================================== -->

    <?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-primary alert-dismissible fade show" role="alert">
        <strong> Sukses !</strong> <?= $this->session->flashdata('success'); ?>
        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </a>
    </div>
    <?php } ?>
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 float-left">Data Pegawai</h5>
            <a href="<?= base_url('pegawai/add') ?>" class="btn btn-primary btn-sm float-right"><i
                    class="fas fa-plus"></i> Tambah Pegawai</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered second" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Gender</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Departemen</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach ($data as $d) { ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $d->nip ?></td>
                            <td><?= $d->nama ?></td>
                            <td><?= $d->gender ?></td>
                            <td><?= $d->tmp_lahir ?></td>
                            <td><?= $d->tgl_lahir ?></td>
                            <td><?= $d->alamat ?></td>
                            <td><?= $d->dep_nama ?></td>
                            <td class="align-center">
                                <a href="<?= base_url('pegawai/edit/'.$d->id) ?>" class="btn btn-warning btn-sm"><i
                                class="fas fa-pencil-alt"></i> Edit</a>
                                <a   onclick="return confirm('Apakah data ini akan dihapus ?')"  href="<?= base_url('pegawai/delete/'.$d->id) ?>" class="btn btn-danger btn-sm"><i
                                class="fas fa-trash"></i> Hapus</a>
                            </td>
                        </tr>

                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end data table  -->
    <!-- ============================================================== -->