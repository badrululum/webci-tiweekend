<div class="row">
    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
        <div class="card">
            <h5 class="card-header">Form Barang</h5>
            <div class="card-body">
                <form action="<?= base_url('barang/add') ?>" method="post">
                    <div class="form-group">
                        <label for="kode"> Kode Barang</label>
                        <input id="kode" type="text" name="kode" data-parsley-trigger="change" required
                            placeholder="Masukan kode" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="nama"> Nama</label>
                        <input id="nama" type="text" name="nama" data-parsley-trigger="change" required
                            placeholder="Masukan nama" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="datetimepicker4"> Tanggal perolehan</label>
                        <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                            <input type="text" name="tgl_perolehan" class="form-control datetimepicker-input"
                                data-target="#datetimepicker4"/>
                            <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="harga_perolehan"> Harga perolehan</label>
                        <input id="harga_perolehan" type="text" name="harga_perolehan" data-parsley-trigger="change"
                            required placeholder="Masukan harga_perolehan" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="merk"> Merk</label>
                        <input id="merk" type="text" name="merk" data-parsley-trigger="change" required
                            placeholder="Masukan merk" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="kategori_id">Kategori</label>
                        <select class="form-control" id="kategori_id" name="kategori_id">
                            <option selected disabled> --pilih kategori-- </option>
                            <?php foreach($kategori as $d) { ?>
                            <option value="<?= $d->id ?>"><?= $d->nama ?></option>
                            <?php } ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="jenis_id">Jenis</label>
                        <select class="form-control" id="jenis_id" name="jenis_id">
                            <option selected disabled> --pilih jenis-- </option>
                            <?php foreach($jenis as $d) { ?>
                            <option value="<?= $d->id ?>"><?= $d->nama ?></option>
                            <?php } ?>
                        </select>
                    </div>


                    <div class="form-group">
                        <button type="submit" name="submit" class="btn btn-primary"><i
                                class="fas fa-location-arrow"></i>
                            Simpan</button>
                        <a href="<?= base_url('pegawai') ?>" class="btn btn-warning"><i class="fas fa-history"></i>
                            Kembali</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>