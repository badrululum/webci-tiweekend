    <!-- ============================================================== -->
    <!-- data table  -->
    <!-- ============================================================== -->

    <?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-primary alert-dismissible fade show" role="alert">
        <strong> Sukses !</strong> <?= $this->session->flashdata('success'); ?>
        <a href="#" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </a>
    </div>
    <?php } ?>
    <div class="card">
        <div class="card-header">
            <h5 class="mb-0 float-left">Data barang</h5>
            <a href="<?= base_url('barang/add') ?>" class="btn btn-primary btn-sm float-right"><i
                    class="fas fa-plus"></i> Tambah barang</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered second" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Tanggal Perolehan</th>
                            <th>Harga Perolehan</th>
                            <th>Merk</th>
                            <th>Kategori</th>
                            <th>Jenis</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php $no=1; foreach ($barang as $b) { ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><?= $b->kode ?></td>
                            <td><?= $b->nama ?></td>
                            <td><?= $b->tgl_perolehan ?></td>
                            <td><?= $b->harga_perolehan ?></td>
                            <td><?= $b->merk ?></td>
                            <td><?= $b->kategori_nama ?></td>
                            <td><?= $b->jenis_nama ?></td>
                            <td class="align-center">
                                <a href="<?= base_url('barang/edit/'.$b->id) ?>" class="btn btn-warning btn-sm"><i
                                        class="fas fa-pencil-alt"></i> Edit</a>
                                <a onclick="return confirm('Apakah data ini akan dihapus ?')"
                                    href="<?= base_url('barang/delete/'.$b->id) ?>" class="btn btn-danger btn-sm"><i
                                        class="fas fa-trash"></i> Hapus</a>
                        </tr>

                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end data table  -->
    <!-- ============================================================== -->