<div class="nav-left-sidebar">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Menu
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= $this->uri->segment(1) == 'dashboard' ? 'active' : null ?>" href="/dashboard">Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= $this->uri->segment(1) == 'departemen' ? 'active' : null ?>" href="/departemen">Departemen</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= $this->uri->segment(1) == 'pegawai' ? 'active' : null ?>" href="/pegawai">Pegawai</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link <?= $this->uri->segment(1) == 'barang' ? 'active' : null ?>" href="<?= base_url('barang') ?>">Barang</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>