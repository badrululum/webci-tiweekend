<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{
	public function __construct()
    {
		parent::__construct();
		// $this->load->model('NilaiSiswa_m');
    }
    
    public function index()
    {
        $this->load->view('welcome');
    }

    public function hello()
    {
		$data['siswa']	= 'Ahmad';
		$data['nilai']	= 90;
		// $ns1 = new $this->NilaiSiswa_m();
		$ns1->nama='Budi';
		$ns1->nilai=90;
		$ns1->matkul='Web Programming';
		$data['ns']= $ns1;
        $this->load->view('welcome/hello',$data);
    }
}
