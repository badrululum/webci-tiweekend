<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{   
    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['breadcrumbs1'] = 'Dasboard'; 
        $data['subview'] = 'dashboard/home';
        $this->load->view('layouts/main', $data);
    }
}
