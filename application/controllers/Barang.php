<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('general_m');
    }
    public function index()
    {
        $data['barang'] = $this->general_m->getJoinBarang();
        $data['title'] = 'Barang';
        $data['breadcrumbs1'] = 'Barang'; 
        $data['subview'] = 'barang/home';
        $this->load->view('layouts/main', $data);
    }

    public function add()
    {
        if(is_null($this->input->post('submit'))) {
            $data['title'] = 'Barang';
            $data['breadcrumbs1'] = 'Barang'; 
            $data['breadcrumbs2'] = 'Tambah'; 
            $data['kategori'] = $this->general_m->getData('kategori');
            $data['jenis'] = $this->general_m->getData('jenis_barang');
            $data['subview'] = 'barang/create';
            $this->load->view('layouts/main', $data);
        }else {
            $tgl = date("Y-m-d", strtotime($this->input->post('tgl_perolehan')));
            $data = [
                'kode' => $this->input->post('kode'),
                'nama' => $this->input->post('nama'),
                'tgl_perolehan' => $tgl,
                'harga_perolehan' => $this->input->post('harga_perolehan'),
                'merk' => $this->input->post('merk'),
                'kategori_id' => $this->input->post('kategori_id'),
                'jenis_id' => $this->input->post('jenis_id'),
            ];
            $insert = $this->general_m->insertData('barang',$data);
            $this->session->set_flashdata('success', 'Masukan data barang');
            redirect('barang');
        }
    }

    public function edit($id)
    {
        if(is_null($this->input->post('submit'))) {
            $data['title'] = 'Barang';
            $data['breadcrumbs1'] = 'Barang'; 
            $data['breadcrumbs2'] = 'Tambah';             
            $data['kategori'] = $this->general_m->getData('kategori');
            $data['jenis'] = $this->general_m->getData('jenis_barang');
            $data['barang'] = $this->general_m->getWhere('barang', ['id'=>$id], 'row');
            $data['subview'] = 'barang/edit';
            $this->load->view('layouts/main', $data);
        }else {
            $tgl = date("Y-m-d", strtotime($this->input->post('tgl_perolehan')));
            $data = [
                'kode' => $this->input->post('kode'),
                'nama' => $this->input->post('nama'),
                'tgl_perolehan' => $tgl,
                'harga_perolehan' => $this->input->post('harga_perolehan'),
                'merk' => $this->input->post('merk'),
                'kategori_id' => $this->input->post('kategori_id'),
                'jenis_id' => $this->input->post('jenis_id'),
            ];
            $this->general_m->updateData('barang',$data, 'id', $id);
            $this->session->set_flashdata('success', 'Edit data barang');
            redirect('barang');
        }
    }

    public function delete($id)
    {
        if (!empty($id)) {
            $delete = $this->general_m->deleteData('barang','id', $id);
            if(is_null($delete)){
                $this->session->set_flashdata('success', 'Delete data barang');
                redirect('barang');
            }
        }
    }
}
