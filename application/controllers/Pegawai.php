<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai extends CI_Controller
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model('General_m');
    }

    public function index()
    {
        $data['title'] = 'Pegawai';
        $data['breadcrumbs1'] = 'Pegawai'; 
        $data['breadcrumbs2'] = 'Data'; 
        $data['subview'] = 'pegawai/home';
        $data['data'] = $this->General_m->getJoinPegawai();
        $this->load->view('layouts/main', $data);
    }

    public function add()
    {
        if(is_null($this->input->post('submit'))) {
            $data['title'] = 'Pegawai';
            $data['breadcrumbs1'] = 'Pegawai'; 
            $data['breadcrumbs2'] = 'Tambah'; 
            $data['departemen'] = $this->General_m->getData('departemen');
            $data['subview'] = 'pegawai/create';
            $this->load->view('layouts/main', $data);
        }else {
            // $idPegawai = $this->General_m->getLimit('pegawai','row_array','id','DESC','1');
            $tgl = date("Y-m-d", strtotime($this->input->post('tgl_lahir')));
            $data = [
                'nip' => $this->input->post('nip'),
                'nama' => $this->input->post('nama'),
                'gender' => $this->input->post('gender'),
                'tmp_lahir' => $this->input->post('tmp_lahir'),
                'tgl_lahir' => $tgl,
                'alamat' => $this->input->post('alamat'),
                'dep_id' => $this->input->post('dep_id'),
            ];
            $insert = $this->General_m->insertData('pegawai',$data);
            $this->session->set_flashdata('success', 'Masukan data pegawai');
            redirect('pegawai');
        }
    }

    public function edit($id)
    {
        if(is_null($this->input->post('submit'))) {
            $data['title'] = 'Pegawai';
            $data['breadcrumbs1'] = 'Pegawai'; 
            $data['breadcrumbs2'] = 'Tambah'; 
            $data['departemen'] = $this->General_m->getData('departemen');
            $data['pegawai'] = $this->General_m->getWhere('pegawai', ['id'=>$id], 'row');
            $data['subview'] = 'pegawai/edit';
            $this->load->view('layouts/main', $data);
        }else {
            $tgl = date("Y-m-d", strtotime($this->input->post('tgl_lahir')));
            $data = [
                'nip' => $this->input->post('nip'),
                'nama' => $this->input->post('nama'),
                'gender' => $this->input->post('gender'),
                'tmp_lahir' => $this->input->post('tmp_lahir'),
                'tgl_lahir' => $tgl,
                'alamat' => $this->input->post('alamat'),
                'dep_id' => $this->input->post('dep_id'),
            ];
            $this->General_m->updateData('pegawai',$data, 'id', $id);
            $this->session->set_flashdata('success', 'Edit data pegawai');
            redirect('pegawai');
        }
    }

    public function delete($id)
    {
        if (!empty($id)) {
            $delete = $this->General_m->deleteData('pegawai','id', $id);
            if(is_null($delete)){
                $this->session->set_flashdata('success', 'Delete data pegawai');
                redirect('pegawai');
            }
        }
    }



}
