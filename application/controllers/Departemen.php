<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Departemen extends CI_Controller
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model('General_m');
    }

    public function index()
    {
        $data['title'] = 'Departemen';
        $data['breadcrumbs1'] = 'Departemen'; 
        $data['breadcrumbs2'] = 'Data'; 
        $data['subview'] = 'departemen/home';
        $data['data'] = $this->General_m->getData('departemen');
        $this->load->view('layouts/main', $data);
    }

    public function add()
    {
        if(is_null($this->input->post('submit'))) {
            $data['title'] = 'Departemen';
            $data['breadcrumbs1'] = 'Departemen'; 
            $data['breadcrumbs2'] = 'Tambah'; 
            $data['departemen'] = $this->General_m->getData('departemen');
            $data['subview'] = 'departemen/create';
            $this->load->view('layouts/main', $data);
        }else {
            $data = [
                'nama' => $this->input->post('nama'),
                'abbr' => $this->input->post('abbr'),
                'alamat' => $this->input->post('alamat'),
                'telpon' => $this->input->post('telepone'),
            ];
            $insert = $this->General_m->insertData('departemen',$data);
            $this->session->set_flashdata('success', 'Masukan data departemen');
            redirect('departemen');
        }
    }

    public function edit($id)
    {
        if(is_null($this->input->post('submit'))) {
            $data['title'] = 'Departemen';
            $data['breadcrumbs1'] = 'Departemen'; 
            $data['breadcrumbs2'] = 'Edit';
            $data['departemen'] = $this->General_m->getWhere('departemen', ['id'=>$id], 'row');
            $data['subview'] = 'departemen/edit';
            $this->load->view('layouts/main', $data);
        }else {
            $data = [
                'nama' => $this->input->post('nama'),
                'abbr' => $this->input->post('abbr'),
                'alamat' => $this->input->post('alamat'),
                'telpon' => $this->input->post('telepone'),
            ];
            $this->General_m->updateData('departemen',$data, 'id', $id);
            $this->session->set_flashdata('success', 'Update data departemen');
            redirect('departemen');
        }
    }
    
    public function delete($id)
    {
        if (!empty($id)) {
            $delete = $this->General_m->deleteData('departemen','id', $id);
            if(is_null($delete)){
                $this->session->set_flashdata('success', 'Delete data departemen');
                redirect('departemen');
            }
        }
    }

}